## URL Shortener

The following image presents the general architecture and data flows of the URL Shortner:

![architecture](architecture.png "Architecture")

It includes some components and workflows.

### The Shinker Worflow

When a user submits a URL for shortening, the following workflow happens:

* the Shortner Service generates a short version of that URL and Publish is the URL and Shortened Version to a Kafka Topic (this topic is replicatted and we use acks=all, so we wait for ACK's across the defined ISR).
* on success the shortened URL is presented as a response.
* the Cache Updater Service listens to the Kafka Topic and Updates an LRU Cache with the submitted new (url, shortened) tupler.
* in paraller the Storage Updater listens to the same Kaka Topic and updates a permanent store with the received new entries.

### The Resolver Workflow

When a user submits a Shortened URL for resolving, the following workflow happens:

* The Resolver service queries the LRU Cache for this Shortened URL.
    * If it does not exist, the Resolver tries to fetch it from the Permanent Store.
    * If it also does not exist the Resolver returns a Not Found.
* The LRU Cache is updated with the (Shortened, URL) Tuple.
* A Hit Message is published to a Kafka Topic (this topic is replicated but we are using ack=0 so we don't wait for acks)
* A Hit Updater Consumer, reads these Hit Messages and aggregates them as hit sums for the resolved URL, this data will then be later used by reporting services.

### URL Shortening algorithm

The URL Shortening algorithm must assure that the resolved shortened algorithm is not easily guessed via incrementing the Base62 token.

To avoid this our algorithm uses N Shards, each shard has a 3DES Encrypter with a specific key.

Then for each URL the process applies the following steps:

* Gets a new sequence number from the Sequencer for the URL
* Gets a Shard by applying modulus N ( where N is the number of shards )
* Encrypts the Sequence Number (64 bit int) using the Shard Encrypter.
* Encodes the resulting encrypted number into a Base62 string
* Appends the Shard Number, encoded using the base62 alphabet to the encoded string.

**Note:**

Regarding Shard Encoding, you can configure the system to have up to 62 shards the translation for these shards whill be:

| ShardID | Encoded |
|---------|---------|
|       0 |       0 |
|     ... |     ... |
|       9 |       9 |
|      10 |       a |
|     ... |     ... |
|     ... |     ... |
|       Z |      61 |

The base configuration will have 8 Shards defined, notice these Shards are encryption shards not storage shards.

### Limitations

Maximum number of possible URL's: 18446744073709551615 

if we use a sequencer per shard and we use 62 Shards we can go up to 62 * 18446744073709551615 URL's

