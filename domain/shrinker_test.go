package domain

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mandalore/surl/domain/encoding"
	models "gitlab.com/mandalore/surl/domain/models"
)

var (
	ctx  = context.Background()
	tURL = "https://google.com/stuff/2343?p=10"
)

func TestShrink(t *testing.T) {
	testCases := []struct {
		description string
		url         string
		expected    string
		shardFinder func() *MockShardFinder
		sequencer   func() *MockSequencer
		storer      func() *MockStorer
		err         assert.ErrorAssertionFunc
	}{
		{
			description: "when sequencer fails",
			err:         assert.Error,
			sequencer: func() *MockSequencer {
				s := &MockSequencer{}

				s.On("Next", ctx).Return(uint64(0), fmt.Errorf("failed to get next sequence"))

				return s
			},
			shardFinder: func() *MockShardFinder {
				return &MockShardFinder{}
			},
			storer: func() *MockStorer {
				return &MockStorer{}
			},
		},
		{
			description: "when shardfinder fails",
			err:         assert.Error,
			url:         tURL,
			sequencer: func() *MockSequencer {
				s := &MockSequencer{}

				s.On("Next", ctx).Return(uint64(1232), nil)

				return s
			},
			shardFinder: func() *MockShardFinder {
				f := &MockShardFinder{}

				f.On("ShardFor", ctx, tURL).Return(models.Shard{}, fmt.Errorf("failed to get shard"))

				return f
			},
			storer: func() *MockStorer {
				return &MockStorer{}
			},
		},
		{
			description: "when store fails",
			err:         assert.Error,
			url:         tURL,
			sequencer: func() *MockSequencer {
				s := &MockSequencer{}

				s.On("Next", ctx).Return(uint64(666), nil)

				return s
			},
			shardFinder: func() *MockShardFinder {
				f := &MockShardFinder{}

				encoder, err := encoding.NewEncoder([]byte("876543211234567812345678"))
				if err != nil {
					panic(err)
				}

				f.On("ShardFor", ctx, tURL).Return(models.Shard{
					ID:      uint8(23),
					Encoder: encoder,
				}, nil)

				return f
			},

			storer: func() *MockStorer {
				s := &MockStorer{}

				s.On("Store", ctx, "CYnBYoD3grT", tURL).Return(fmt.Errorf("failed"))

				return s
			},
		},
		{
			description: "when store succeeds",
			err:         assert.NoError,
			url:         tURL,
			expected:    "CYnBYoD3grT",
			sequencer: func() *MockSequencer {
				s := &MockSequencer{}

				s.On("Next", ctx).Return(uint64(666), nil)

				return s
			},
			shardFinder: func() *MockShardFinder {
				f := &MockShardFinder{}

				encoder, err := encoding.NewEncoder([]byte("876543211234567812345678"))
				if err != nil {
					panic(err)
				}

				f.On("ShardFor", ctx, tURL).Return(models.Shard{
					ID:      uint8(23),
					Encoder: encoder,
				}, nil)

				return f
			},

			storer: func() *MockStorer {
				s := &MockStorer{}

				s.On("Store", ctx, "CYnBYoD3grT", tURL).Return(nil)

				return s
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			shrinker := NewShrinker(tc.sequencer(), tc.shardFinder(), tc.storer())
			res, err := shrinker.Shrink(ctx, tc.url)

			tc.err(t, err, "should match error assertion")
			assert.Equal(t, res, tc.expected, "should have returned expected short url")
		})
	}
}
