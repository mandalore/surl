package domain

import (
	"context"
	"fmt"

	"gitlab.com/mandalore/surl/domain/models"
)

// ShardFinder interface defining methods for URL to Shard mapping
type ShardFinder interface {
	ShardFor(ctx context.Context, url string) (models.Shard, error)
}

// Sequencer interface providing a sequencer
type Sequencer interface {
	Next(ctx context.Context) (uint64, error)
}

// Storer interface providing storing shorten url features
type Storer interface {
	Store(ctx context.Context, surl, url string) error
}

// Shrinker ...
type Shrinker struct {
	sequencer Sequencer
	shards    ShardFinder
	store     Storer
}

// NewShrinker factory method that creates a new Service for shrinking URLs
func NewShrinker(sequencer Sequencer, shards ShardFinder, store Storer) Shrinker {
	return Shrinker{
		sequencer: sequencer,
		shards:    shards,
		store:     store,
	}
}

// Shrink receives a url, generates a short version of this url and stores it
func (s Shrinker) Shrink(ctx context.Context, url string) (string, error) {
	seq, err := s.sequencer.Next(ctx)
	if err != nil {
		return "", fmt.Errorf("failed to fetch sequence: %w", err)
	}

	shard, err := s.shards.ShardFor(ctx, url)
	if err != nil {
		return "", fmt.Errorf("failed to identify shard for %s: %w", url, err)
	}

	surl := shard.Encoder.Encode(seq)

	if err := s.store.Store(ctx, surl, url); err != nil {
		return "", fmt.Errorf("failed to store url %s for shard for %d: %w", url, shard.ID, err)
	}

	return surl, nil
}
