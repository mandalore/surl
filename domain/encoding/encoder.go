package encoding

import (
	"crypto/cipher"
	"crypto/des"
	"encoding/binary"
	"fmt"

	"github.com/nicksnyder/basen"
)

// Encoder type representing a Base62 <-> uint64 encoder
type Encoder struct {
	cipher cipher.Block
}

// EncoderOption type for first orde function that defines Optional params for the encoder
type EncoderOption func(e *Encoder) error

// NewEncoder factory method to create a new encoder
func NewEncoder(key []byte) (Encoder, error) {
	//c, err := aes.NewCipher(key)
	c, err := des.NewTripleDESCipher(key)
	if err != nil {
		return Encoder{}, fmt.Errorf("failed to create tripleDES cipher: %v", err)
	}

	return Encoder{cipher: c}, nil
}

// Encode converts an int64 to a base62 string
func (e Encoder) Encode(v uint64) string {
	src := make([]byte, 8)
	dst := make([]byte, 8)
	binary.BigEndian.PutUint64(src, v)

	e.cipher.Encrypt(dst, src)

	return basen.Base62Encoding.EncodeToString(dst)
}

// Decode converts a string into an uint64
func (e Encoder) Decode(u string) uint64 {
	dst := make([]byte, 8, 8)

	src, err := basen.Base62Encoding.DecodeString(u)
	if err != nil {
		return 0
	}

	if len(src) < e.cipher.BlockSize() {
		var missing = e.cipher.BlockSize() - len(src)
		src = append(make([]byte, missing, missing), src...)
	}

	e.cipher.Decrypt(dst, src)

	return binary.BigEndian.Uint64(dst)
}
