package encoding

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	key = []byte("123456781234567812345678")
)

func TestEncoder(t *testing.T) {
	e, err := NewEncoder(key)
	assert.NoError(t, err, "no error returned")

	assert.Equal(t, e.Encode(12312), "GPbLcFSWOrn", "should have encrypted the number correctly")
	assert.Equal(t, e.Encode(34323), "8Bh0mCPG5qk", "should have encrypted the number correctly")
	assert.Equal(t, e.Decode("8Bh0mCPG5qk"), uint64(34323), "should have decoded the int correctly")
	assert.Equal(t, e.Decode("GPbLcFSWOrn"), uint64(12312), "should have decoded the int correctly")

	for i := 0; i <= 1000; i++ {
		seq := uint64(rand.Uint64())
		s := e.Encode(seq)
		assert.Greater(t, len(s), 0, "should return a non-empty string")
		assert.Equal(t, seq, e.Decode(s), "should have decoded correctly")
	}
}
