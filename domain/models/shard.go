package models

import "gitlab.com/mandalore/surl/domain/encoding"

// Shard defines a Shard entry
type Shard struct {
	ID      uint8
	Encoder encoding.Encoder
}

func NewShard(ID uint8, encoder encoding.Encoder) Shard {
	return Shard{}
}
