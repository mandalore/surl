PACKAGES=$(shell go list ./... | grep -v /vendor/ | grep -v mock | grep -v /tests) 

check:
	@echo "go vet"
	go vet -all ./...

env:
	docker-compose up

mocks:
	@rm -rf mock_*
	mockery -dir domain -all -inpkg -testonly

unit-tests: 
	@go test -race -short -cover $(PACKAGES)

local: env mocks unit-tests

test: install mocks
	@echo "Running tests"
	@go test -coverprofile=.coverage.out -timeout 30s ./...
	go tool cover -func=.coverage.out | tail -1 

local-test: env test

