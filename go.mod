module gitlab.com/mandalore/surl

go 1.13

require (
	github.com/lytics/base62 v0.0.0-20180808010106-0ee4de5a5d6d
	github.com/mattheath/base62 v0.0.0-20150408093626-b80cdc656a7a
	github.com/nicksnyder/basen v1.0.0
	github.com/segmentio/fasthash v1.0.1
	github.com/stretchr/testify v1.5.1
)
